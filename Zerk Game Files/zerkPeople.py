from Game import Game

theGame = Game()
theGame.load_paths()
theGame.load_places()
theGame.load_npcs()
theGame.run()
