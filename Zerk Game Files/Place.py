class Place:
    
    #name = string, description = string, exits = list of paths. 
    def __init__(self,name,description,exits,npcs):
        self.name = name
        self.description = description
        self.exits = exits
        self.npcs = npcs

    def render(self):
        print(self.name)
        print(self.description)
        for npc in self.npcs:
            npc.render()
        for path in self.exits:
            path.render()
       
