import random

class NPC:

    def __init__(self,name,description,location,greeting,deadDesc,hp,friendly,maxDamage,loot):
        self.name = name
        self.description = description
        self.location = location
        self.greeting = greeting
        self.deadDesc = deadDesc
        self.hp = hp
        self.friendly = friendly
        self.maxDamage = maxDamage
        self.loot = loot
        self.currentLocation = ""

    def attack(self):
        return random.randint (0, self.maxDamage)

    def alive(self):
        if (self.hp > 1):
            return True
        else:
            self.hp = 0
            return False

    def render(self):
        print(self.name)
        if(self.alive()):
            print(self.description)
            print(self.name + " says " + self.greeting)
            if(self.friendly):
                print("This person doesn't seem very aggressive")
            else:
                print("This person seem aggressive. HP: " + str(self.hp))
        else:
            print(self.deadDesc)
            print("They have dropped some loot, you should take it!")

