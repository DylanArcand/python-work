class Path:

    # all parameters are strings.  
    def __init__(self,descriptionString,keyCommandString,destinationName):
        self.description = descriptionString
        self.keyCommand  = keyCommandString
        self.destination = destinationName
        
        
    def render(self):
        print ("[ " + self.keyCommand + " ] " + self.description)


