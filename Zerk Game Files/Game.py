import csv


from Path   import Path
from Place  import Place
from Player import Player
from Player import NPC

class Game:

    def __init__(self):
        # stores all the places in the game
        self.places = {}
        self.paths =  {}
        self.npcs = {}
        self.player = Player("Heck", 100)
            
    def load_places(self):   
        filename = "Places.csv"
        file = open(filename)
        reader = csv.reader(file)   # object containing the rows

        for row in reader:          # each row is a list
            name = row[0]
            description = row[1]
            place_exits = []
            npcs = []
            rowLength = len(row)
            place = Place(name, description, place_exits,npcs)
            self.places[name] = place
            numberOfExits = rowLength -2
            exitIndex = 2

            while exitIndex < rowLength:
                exitName = row[exitIndex]
                exitPath = self.paths[exitName]
                place_exits.append(exitPath)
                exitIndex += 1

    def load_paths(self):
        filename = "Exits.csv"
        file = open(filename)
        reader = csv.reader(file)        
   
        for row in reader:
            # row = name, description
            name = row[0]
            description = row[1]
            keyCommandString = row[2]
            destination = row[3]
            rowLength = len(row)
            path = Path(description, keyCommandString, destination)
            self.paths[name] = path


    def load_npcs(self):
            filename = "NPC.csv"
            file = open(filename)
            reader = csv.reader(file)        
       
            for row in reader:
                # row = name, description
                name = row[0]
                description = row[1]
                location = row[2]
                greeting = row[3]
                deadDesc = row[4]
                hp = int(row[5])
                friendly = bool(int(row[6]))
                maxDamage = int(row[7])
                loot = row[8]
                npcs = []
                rowLength=len(row)
                npc = NPC(name, description, location, greeting, deadDesc, hp, friendly, maxDamage, loot) # CREATES AN NPC OUTPUT
                self.npcs[name] = npc 

    def run(self):

        while(1):

            HP = self.player.render()
            P = self.places[self.player.locationName]
            P.render()

            roomPerson = None

            for person_name in self.npcs:
                person = self.npcs[person_name] 
                if (person.locationName == P.name):
                    roomPerson = person

            if (roomPerson):
                roomPerson.render()
            
            player_action = input("-->")

            if(player_action == "inventory"):
                print("Here's your shit, fuck bag")
                for item in self.player.inventory:
                    print(item)

            if(player_action == "take"):
                if(roomPerson.alive() == False):
                    if(roomPerson.name == "Hot Tamale Fireball"):
                        i = 0
                        for item in self.player.inventory:
                            if (item == "Fireball"):
                                i = 1
                        if(i == 1):
                            print("Nothing to take")
                            continue

                        
                    self.player.inventory.append(roomPerson.loot)
                else:
                    print("Nothing to take")


            if(player_action == "attack"):
                if(roomPerson == None):
                    print("You hit nothing because no one is there, your fist does make a cool noise in the wind though")
                elif(roomPerson.friendly == True):
                    print("Please don't attack people who have no issues with you")
                elif(roomPerson.alive() == False):
                    print(roomPerson.name + " is dead, quit trying to kill them again")
                else:
                    player_damage = self.player.attack()
                    print("You deal " + str(player_damage) + " damage to your opponent!")
                    npc_damage = roomPerson.attack()
                    print(roomPerson.name + " deals " + str(npc_damage) + " damage to you!")

                    self.player.hp -= npc_damage
                    roomPerson.hp -= player_damage

            for E in P.exits:
                if ( E.keyCommand == player_action):                  
                    self.player.locationName = E.destination
