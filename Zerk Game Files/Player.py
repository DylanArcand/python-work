import random

# from Character import Character

class Character:

    def __init__(self,name,hp,maxDamage,locationName):
        self.name = name
        self.hp = hp
        self.maxDamage = maxDamage
        self.locationName = locationName

class Player(Character):

    def __init__(self,locationeName, hp):
        super().__init__( "The Player", 100, 20, "Heck")

        self.inventory = []

    def attack(self):
        for item in self.inventory:
            if(item == "Fireball"):
                return random.randint(0, 35)
        
        return random.randint(0, 20)

    def render(self):
        print("Current HP: " + str(self.hp))
        if self.hp < 1:
            print("Game over nerd")
            self.currentPlaceName = "Heck"
            self.hp = 100

class NPC(Character):

    def __init__(self,name,description,locationName,greeting,deadDesc,hp,friendly,maxDamage,loot):
        super().__init__( name, hp, maxDamage, locationName)
        self.name = name
        self.description = description
        self.locationName = locationName
        self.greeting = greeting
        self.deadDesc = deadDesc
        self.hp = hp
        self.friendly = friendly
        self.maxDamage = maxDamage
        self.loot = loot
        self.currentLocation = ""

    def attack(self):
        return random.randint (0, self.maxDamage)

    def alive(self):
        if (self.hp > 1):
            return True
        else:
            self.hp = 0
            return False

    def render(self):
        print(self.name)
        if(self.alive()):
            print(self.description)
            print(self.name + " says " + self.greeting)
            if(self.friendly):
                print("This person doesn't seem very aggressive")
            else:
                print("This person seem aggressive. HP: " + str(self.hp))
        else:
            print(self.deadDesc)
            print("They have dropped some loot, you should take it!")
