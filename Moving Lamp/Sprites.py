import pygame

class BigBoySprites(pygame.sprite.Sprite):

    def __init__(self, ImageName):
        pygame.sprite.Sprite.__init__(self)
        #Image is a surface
        self.image = pygame.image.load(ImageName)
        self.rect = self.image.get_rect()

    def set_center_position(self,x,y):
        
        self.rect.center = (x,y)

sprite1 = BigBoySprites("32x32 Star.png")
sprite2 = BigBoySprites("Background.jpg")
