import pygame
import sys
import random
from Sprites import BigBoySprites

Length = 800
Height = 600

BlockWidth = 100
BlockHeight = 32


clock = pygame.time.Clock()

pygame.mixer.init()

Crashing = pygame.mixer.Sound('Crashing.wav')
pygame.mixer.music.load("Background Music.mp3")

pygame.mixer.music.set_volume(.3)


    
    

#Transparent image offsets
xPositive = 0
yPositive = 0
xNegative = Length -32
yNegative = Height -32

MoveSpeed = 5

gameDisplay = pygame.display.set_mode((Length,Height))
pygame.display.set_caption('Wild West Adventure')

NiceBlue   = [94, 144, 224]
NiceMint   = [29, 229, 129]
NiceYellow = [229, 222, 28]
NicePurple = [119, 49, 145]
LightPink = [226, 136, 163]
Black = [0,0,0]

#gameDisplay.fill(NiceMint)

#Lamp = pygame.image.load('lamp.png')
Player = BigBoySprites("32x32 Star.png")
Background = BigBoySprites("Background.jpg")

SpriteGroup1 = pygame.sprite.Group()
SpriteGroup1.add(Player)

SpriteGroupBackground = pygame.sprite.Group()
SpriteGroupBackground.add(Background)
#SpriteGroup.add(Lamp2)

class Block(pygame.sprite.Sprite):

    def __init__(self, screen, width, height):
        super().__init__()
        self.image = pygame.Surface((width, height))
        self.image.fill(Black)
        self.screen = screen
        self.rect = self.image.get_rect()
        self.fallspeed = 2

    def update(self):
        self.rect.y += self.fallspeed

    def collide(self, spriteGroup):
        if pygame.sprite.spritecollide(self, spriteGroup, False):
            Crashed()
            spriteGroup.empty()

def LampConstructor(x,y):
    Background.rect.x = -150
    Background.rect.y = -375
    Player.rect.x = x
    Player.rect.y = y
    SpriteGroupBackground.draw(gameDisplay)
    SpriteGroup1.draw(gameDisplay)#.blit(Lamp, (x,y))

    
Blocks1 = pygame.sprite.Group()
Blocks2 = pygame.sprite.Group()
Blocks3 = pygame.sprite.Group()
Blocks4 = pygame.sprite.Group()

def Blockers(SpriteGroup):

    Index = 0
    OpenSpace = random.randint(0,7)
    
    for x in range(8):
        Blocker = Block(gameDisplay, BlockWidth, BlockHeight)
        Blocker.rect.x = Index * 100
        if OpenSpace == Index:
            Blocker.rect.x = -100
        Index += 1
        Blocker.rect.y = -50
        SpriteGroup.add(Blocker)
    
def Crashed():
        pygame.mixer.Sound.set_volume(Crashing,.1)
        pygame.mixer.Sound.play(Crashing)


x = (-16 + (Length * .5))# + 100
y = (-16 + (Height * .5))# + 100

def CollisionDetection():
    for sprite in Blocks1:
        Blocks1.remove(sprite)
        Block.collide(sprite, SpriteGroup1)
        Blocks1.add(sprite)

    for sprite in Blocks2:
        Blocks2.remove(sprite)
        Block.collide(sprite, SpriteGroup1)
        Blocks2.add(sprite)

    for sprite in Blocks3:
        Blocks3.remove(sprite)
        Block.collide(sprite, SpriteGroup1)
        Blocks3.add(sprite)

    for sprite in Blocks4:
        Blocks4.remove(sprite)
        Block.collide(sprite, SpriteGroup1)
        Blocks4.add(sprite)

pygame.display.update()

gameExit = False

pygame.mixer.music.play(-1)

SpawnClock = 0

while not gameExit:

    clock.tick(60)
        
    SpawnClock += 1
    SpawnWave=1

    if(SpawnWave == 5):
        SpawnWave=1

    if(SpawnClock == 90):
        if(SpawnWave==1):
            Blockers(Blocks1)
            
        if(SpawnWave==2):
            Blockers(Blocks2)
            
        if(SpawnWave==3):
            Blockers(Blocks3)
            
        if(SpawnWave==4):
            Blockers(Blocks4)
            
        SpawnWave+=1
        SpawnClock = 0
            
    
    keys = pygame.key.get_pressed()
    
    CollisionDetection()
    
    if keys[pygame.K_LEFT]:
        #gameDisplay.fill(NiceBlue)
        x -= MoveSpeed
                
    if keys[pygame.K_RIGHT]:
        #gameDisplay.fill(NiceMint)
        x += MoveSpeed
                
    if keys[pygame.K_UP]:
        #gameDisplay.fill(NiceYellow)
        y -= MoveSpeed
                
    if keys[pygame.K_DOWN]:
        #gameDisplay.fill(NicePurple)
        y += MoveSpeed
                
    if keys[pygame.K_SPACE]:
        x = (-16 + (Length * .5))
        y = (-16 + (Height * .5))
        pygame.mixer.Sound.set_volume(Crashing,1)
        pygame.mixer.Sound.play(Crashing)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameExit = True 

    #Barriors to account for image offsets
    if(x < xPositive):
        x = xPositive
    if(y < yPositive):
        y = yPositive
    if(x > xNegative):
        x = xNegative
    if(y > yNegative):
        y = yNegative



    gameDisplay.fill(NiceBlue)
    LampConstructor(x,y)
    Blocks1.update()
    Blocks1.draw(gameDisplay)
    Blocks2.update()
    Blocks2.draw(gameDisplay)
    Blocks3.update()
    Blocks3.draw(gameDisplay)
    Blocks4.update()
    Blocks4.draw(gameDisplay)
    pygame.display.update()


pygame.quit()
quit()
